# Keras examples directory

## General usage

[addition_rnn.py](keras/general/addition_rnn.py)
Implementation of sequence to sequence learning for performing addition of two numbers (as strings).

[antirectifier.py](keras/general/antirectifier.py)
Demonstrates how to write custom layers for Keras.

[stateful_lstm.py](keras/general/stateful_lstm.py)
Demonstrates how to use stateful RNNs to model long sequences efficiently.

[variational_autoencoder.py](keras/general/variational_autoencoder.py)
Demonstrates how to build a variational autoencoder.

## Natural Language Processing

[babi_memnn.py](keras/nlp/babi_memnn.py)
Trains a memory network on the bAbI dataset for reading comprehension.

[babi_rnn.py](keras/nlp/babi_rnn.py)
Trains a two-branch recurrent network on the bAbI dataset for reading comprehension.

[imdb_bidirectional_lstm.py](keras/nlp/imdb_bidirectional_lstm.py)
Trains a Bidirectional LSTM on the IMDB sentiment classification task.

[imdb_cnn.py](keras/nlp/imdb_cnn.py)
Demonstrates the use of Convolution1D for text classification.

[imdb_cnn_lstm.py](keras/nlp/imdb_cnn_lstm.py)
Trains a convolutional stack followed by a recurrent stack network on the IMDB sentiment classification task.

[imdb_fasttext.py](keras/nlp/imdb_fasttext.py)
Trains a FastText model on the IMDB sentiment classification task.

[imdb_lstm.py](keras/nlp/imdb_lstm.py)
Trains a LSTM on the IMDB sentiment classification task.

[lstm_benchmark.py](keras/nlp/lstm_benchmark.py)
Compares different LSTM implementations on the IMDB sentiment classification task.

[lstm_text_generation.py](keras/nlp/lstm_text_generation.py)
Generates text from Nietzsche's writings.

[pretrained_word_embeddings.py](keras/nlp/pretrained_word_embeddings.py)
Loads pre-trained word embeddings (GloVe embeddings) into a frozen Keras Embedding layer, and uses it to train a text classification model on the 20 Newsgroup dataset.

[reuters_mlp.py](keras/nlp/reuters_mlp.py)
Trains and evaluate a simple MLP on the Reuters newswire topic classification task.

[reuters_mlp_relu_vs_selu.py](keras/nlp/reuters_mlp_relu_vs_selu.py)
Compare ReLU and SeLU activations in MLPs on the Reuters newswire topic classification task.


# Image Processing

[cifar10_cnn.py](keras/imp/cifar10_cnn.py)
Trains a simple deep CNN on the CIFAR10 small images dataset.

[cifar10_cnn_earlystop_tensorboard.py](keras/imp/cifar10_cnn_earlystop_tensorboard.py)
Trains a simple deep CNN on the CIFAR10 small images dataset, with early stopping and tensorboard output.

[conv_filter_visualization.py](keras/imp/conv_filter_visualization.py)
Visualization of the filters of VGG16, via gradient ascent in input space.

[conv_lstm.py](keras/imp/conv_lstm.py)
Demonstrates the use of a convolutional LSTM network with movies.

[deep_dream.py](keras/imp/deep_dream.py)
Deep Dreams in Keras.

[image_ocr.py](keras/imp/image_ocr.py)
Trains a convolutional stack followed by a recurrent stack and a CTC logloss function to perform optical character recognition (OCR).

[mnist_acgan.py](keras/imp/mnist_acgan.py)
Implementation of AC-GAN ( Auxiliary Classifier GAN ) on the MNIST dataset

[mnist_cnn.py](keras/imp/mnist_cnn.py)
Trains a simple convnet on the MNIST dataset.

[mnist_hierarchical_rnn.py](keras/imp/mnist_hierarchical_rnn.py)
Trains a Hierarchical RNN (HRNN) to classify MNIST digits.

[mnist_irnn.py](keras/imp/mnist_irnn.py)
Reproduction of the IRNN experiment with pixel-by-pixel sequential MNIST in "A Simple Way to Initialize Recurrent Networks of Rectified Linear Units" by Le et al.

[mnist_mlp.py](keras/imp/mnist_mlp.py)
Trains a simple deep multi-layer perceptron on the MNIST dataset.

[mnist_net2net.py](keras/imp/mnist_net2net.py)
Reproduction of the Net2Net experiment with MNIST in "Net2Net: Accelerating Learning via Knowledge Transfer".

[mnist_siamese_graph.py](keras/imp/mnist_siamese_graph.py)
Trains a Siamese multi-layer perceptron on pairs of digits from the MNIST dataset.

[mnist_sklearn_wrapper.py](keras/imp/mnist_sklearn_wrapper.py)
Demonstrates how to use the sklearn wrapper.

[mnist_swwae.py](keras/imp/mnist_swwae.py)
Trains a Stacked What-Where AutoEncoder built on residual blocks on the MNIST dataset.

[mnist_tfrecord.py](keras/imp/mnist_tfrecord.py)
Demonstrates how to use MNIST dataset with TFRecords, the standard tensorflow data format.

[mnist_transfer_cnn.py](keras/imp/mnist_transfer_cnn.py)
Transfer learning toy example.

[neural_doodle.py](keras/imp/neural_doodle.py)
Neural doodle.

[neural_style_transfer.py](keras/imp/neural_style_transfer.py)
Neural style transfer.

[variational_autoencoder_deconv.py](keras/imp/variational_autoencoder_deconv.py)
Demonstrates how to build a variational autoencoder with Keras using deconvolution layers.

