import keras
from keras.datasets import cifar10
from keras.callbacks import EarlyStopping, TensorBoard
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras.layers.noise import AlphaDropout
from sklearn.model_selection import train_test_split

import os
import pickle
import numpy as np


# setting model parameters

batch_size = 200
num_classes = 10
epochs = 100
activation = "relu" 
# Available choices: Dropout, AlphaDropout
dropout_fun = Dropout
dropout_val = 0.2
fc_layers = 4
fc_units = 64
callbacks = [
    EarlyStopping(patience=3),
    TensorBoard(
        log_dir="./tblog/dnn_n{}_l{}".format(fc_units, fc_layers),
        batch_size=batch_size,
        write_images=True)
]


# The data, shuffled and split between train, validation and test sets

(x_full, y_full), (x_test, y_test) = cifar10.load_data()

x_full = np.append(x_full, x_test, axis=0)
x_full = x_full.astype('float32') / 255

y_full = np.append(y_full, y_test, axis=0)
y_full = keras.utils.to_categorical(y_full, num_classes)

x_train, x_test, y_train, y_test = train_test_split(x_full, y_full, test_size=0.2)
x_train, x_valid, y_train, y_valid = train_test_split(x_train, y_train, test_size=0.25)

print('x_train shape:', x_train.shape)
print('y_train shape:', y_train.shape)
print(x_train.shape[0], 'train samples')
print(x_valid.shape[0], 'valid samples')
print(x_test.shape[0], 'test samples')

# Build CNN model

model = Sequential()

model.add(Conv2D(32, (3, 3), padding='same',
                 input_shape=x_train.shape[1:]))
model.add(Activation(activation))
model.add(Conv2D(32, (3, 3)))
model.add(Activation(activation))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(dropout_fun(dropout_val))

model.add(Conv2D(64, (3, 3), padding='same'))
model.add(Activation(activation))
model.add(Conv2D(64, (3, 3)))
model.add(Activation(activation))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(dropout_fun(dropout_val))

model.add(Flatten())
for _ in range(fc_layers):
    model.add(Dense(fc_units))
    model.add(Activation(activation))
    model.add(dropout_fun(dropout_val))
model.add(Dense(num_classes))
model.add(Activation('softmax'))
optimizer = keras.optimizers.Adam()
model.compile(loss='categorical_crossentropy',
              optimizer=optimizer,
              metrics=['accuracy'])
print(model.summary())


# Train the model with training and validation sets

model.fit(x_train, y_train,
              batch_size=batch_size,
              epochs=epochs,
              validation_data=(x_valid, y_valid),
              callbacks=callbacks,
              shuffle=True)

# Test results
print("Test loss: {0[0]}\nTest accuracy: {0[1]:.2%}".format(model.evaluate(x_test, y_test, batch_size=batch_size)))

